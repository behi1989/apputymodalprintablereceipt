/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.{html,js}"],
  theme: {
    extend: {
      fontFamily: {
        YekanBakh: ['YekanBakh']
      },
      colors: {
        cNavyBlue: "#2C285A",
        cmGray: "#D3CDC6",
        cmTextDark: "#414042",
        cmTextLight: "#6B6B6B",
        gray: {
          350: "rgba(44, 40, 90, 0.1)"
        }
      },
    },
  },
  plugins: [],
}
